// import { createRouter, createWebHistory } from 'vue-router'
import Vue from "vue"
import Router from "vue-router" 
//引入组件
import Login from '../components/Login.vue'
import Home from '../components/Home.vue'

Vue.use(Router)
const router =new Router({
  routes:[
    // 路由重定向  在根路径时跳转login
    {path:'/',redirect:'login'}, 
    {
      path: '/login',
      name: 'Login',
      component: Login},
      {
        path: '/home',
        name: 'Home',
        component: Home}
  ]
}) 
// 挂载路由守卫
router.beforeEach((to,from,next)=>{
  //to:将要访问的路径；from:从哪个路径来；
    //next是一个函数，表示发行:  next()放行 next('/login') 强制跳转'/login'
    if(to.path==='/login'){return next()} //如果去登录页，直接放行
    //获取token
    let tokenStr=window.sessionStorage.getItem('token')
    if(!tokenStr){return next('/login')}else{next()} //没有token就是没有登陆，就跳转登录页

})
    
  // {
  //   path: '/about',
  //   name: 'About',
  //   //懒加载
  //   component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  // }




export default router  //暴露路由出去
