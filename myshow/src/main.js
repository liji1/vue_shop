// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import ElementUi from "element-ui"
import 'element-ui/lib/theme-chalk/index.css';
//引入icon文件
import './assets/iconfont/iconfont.css'
import axios from 'axios' //引入axios

// axios全局注册 
axios.defaults.baseURL="http://127.0.0.1:8888/api/private/v1" //默认基础地址 根路径
// 配置请求拦截器(请求接口数据，服务器会判断Authorization是否符合要求，不符合就会驳回此次请求)
axios.interceptors.request.use(config=>{ //config就是请求对象
  console.log(config)
  config.headers.Authorization=window.sessionStorage.getItem('token')
  //最后必须retrun
  return config
})
Vue.prototype.$axios=axios //使用this.$axios


Vue.config.productionTip = false
Vue.use(ElementUi)
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
