//功能:服务器程序
//1:引入四个模块
const express = require("express"); //web服务器模块
const mysql = require("mysql");//mysql模块
const session = require("express-session");//session模块
const cors = require("cors");//跨域
//2:创建连接池
var pool = mysql.createPool({
    host:"127.0.0.1",
    user:"root",
    password:"",
    database:"xz",
    port:3306,
    connectionLimit:15
})
//3:创建web服务器
var server = express();
//4:配置跨域模块
//4.1:允许程序列表 脚手架
//4.2:每次请求验证
server.use(cors({
    origin:["http://127.0.0.1:8080","http://localhost:8080"],
    credentials:true 
}))
//5:指定静态资源目录 public
server.use(express.static("public"));
//6:配置session对象 !!!
server.use(session({
    secret:'128位安全字符串',//加密条件
    resave:true,//每次请求更新数据
    saveUninitialized:true,//保存初始化数据
}))
//7:为服务器绑定监听端口 4000
server.listen(4000);
console.log('服务器启动...')

//功能一:用户登录验证
server.get('/login',(req,res)=>{
    var u=req.query.uname;
    var p=req.query.upwd;
    var sql="select id from xz_login where uname=? and upwd=md5(?)";
    pool.query(sql,[u,p],(err,result)=>{
        // console.log(result) //[ RowDataPacket { id: 1 } ]
        if(err) throw err;
        if(result.length==0){
            res.send({code:-1,msg:'用户名或者密码有误！'})
        }else{
            //如果登录成功：创建session对象并且将登录凭证uid保存到对象中  result=[{id：1}]
            req.session.uid=result[0].id; //将当前登录用户id保存session对象中作为 登录凭证
            // console.log(req.session.uid) //1
            res.send({code:1,msg:'登录成功'})
        }
    })
})

//功能二:显示商品列表-（分页）
server.get("/product",(req,res)=>{
    //接收参数 页码 一页几行
    var pno=req.query.pno;
    var ps=req.query.pageSize
    //为参数设置默认值 1  20
    if(!pno){pno=1}
    if(!ps){ps=20}
    // 创建sql语句查询
    var sql="select lid,lname,price,pic from xz_laptop limit ?,?";
    var offset=(pno-1)*ps;
    ps=parseInt(ps);
    //连接池
    pool.query(sql,[offset,ps],(err,result)=>{
        if(err) throw err;
        res.send({code:1,msg:"查询成功",data:result})
    })
})

//功能三：将商品添加至购物车
server.get("/addcart",(req,res)=>{
    // 1.获取当前用户的登录凭证
    // 如果没有凭证 输出请登录(或跳转登录界面)   
    var uid=req.session.uid //在写登录功能是将登录凭证保存在sessioin.uid里
    if(!uid){
        res.send({code:-2,msg:"请先登录"})
        return;
    }
    //2.获取脚手架传递的数据 lid lname price
    var lid=req.query.lid;
    var lname=req.query.lname;
    var price=req.query.price;
    //3.创建sql语句 查询当前用户是否购买过此商品 uid/用户id   lid/购物车商品编号
    var sql = "select id from xz_cart where uid=? and lid=?";
    pool.query(sql,[uid,lid],(err,result)=>{ //p-start
        if(err) throw err;
        //3.1获取查询结果(判断是否购买过此商品)
        if(result.length==0){
            //没有购买过  向数据库里面添加一条商品
            var sql=`insert into xz_cart values(null,${lid},'${lname}',${price},1,${uid})`; //!***!注意lname这里要用单引号
        }else{
            //已经购买过   把当前商品的购买数量count+1
            var sql=`update xz_cart set count=count+1 where uid=${uid} and lid=${lid}`;
        }
        //4执行sql语句 连接池
        pool.query(sql,(err,result)=>{
            if(err) throw err;
            res.send({code:1,msg:"添加成功!"})
        })
    })//p-end
})

//功能四：查询用户购物车列表
server.get("/findcart",(req,res)=>{
    var uid=req.session.uid;
    if(!uid){
        res.send({code:-2,msg:'请登录',data:[]})
        return;
    }
    var sql="select id,lid,lname,price,count from xz_cart where uid=?";
    pool.query(sql,[uid],(err,result)=>{
        if(err) throw err;
        res.send({code:1,msg:"查询成功",data:result})
    })
})

//功能五:删除购物车中一条商品信息
server.get("/del",(req,res)=>{
    //1:获取用户登录凭证
    var uid = req.session.uid;
    //2:如果没有登录凭证 请登录
    if(!uid){
       res.send({code:-2,msg:"请登录"});
       return;
    }
    //3:获取脚手架传递数据 id
    var id = req.query.id;
    //4:创建sql依据id删除数据
    var sql = "DELETE FROM xz_cart WHERE id = ? AND uid=?";
    //5:执行sql语句获取返回结果
    pool.query(sql,[id,uid],(err,result)=>{
         if(err)throw err;
         //判断删除是否成功 affectedRows 影响行数
         if(result.affectedRows>0){
             res.send({code:1,msg:"删除成功"});
         }else{
             res.send({code:-1,msg:"删除失败"});
         }
    });
    //6:将结果返回脚手架
  })
  
  //测试
  // 1:   重新启动 node app.js
  //       select * from xz_cart;
  // 2:   打开浏览器测  http://127.0.0.1:4000/del?id=1
  // 3:   打开浏览器测  http://127.0.0.1:4000/login?uname=tom&upwd=123
  // 4:   打开浏览器测  http://127.0.0.1:4000/del?id=1
  // 5:   打开浏览器测  http://127.0.0.1:4000/del?id=1
  
  
  //功能六：删除用户选中的商品
  server.get("/delm",(req,res)=>{
      var uid=req.session.uid;
      if(!uid){
          res.send({code:-2,msg:"请登录"});
          return;
      }
      var id=req.query.id;
      var sql=`delete from xz_cart where id in (${id})`;
      pool.query(sql,(err,result)=>{
          if(err) throw err;
          if(result.affectedRows>0){
              res.send({code:1,msg:"删除成功"})
          }else{
              res.send({code:-1,msg:"删除失败"})
          }
      })
  })
